#include "StackImpl.h"

#include <stdlib.h>
#include <string.h>
#include <assert.h>

Stack constructStack(StackMethods methods, ItemType type, void *impl) {
    assert(methods);
    assert(type);
    
    Stack new = NULL;
    
    if (impl) {
        stack s = {
            .methods = methods,
            .type = type,
            .impl = impl
        };
        
        new = calloc(1, sizeof(stack));
        if (new) {
            memcpy(new, &s, sizeof(stack));
        } else {
            methods->free(&s);
        }
    }
    
    return new;
}

void StackPush(Stack s, Item e) {
    assert(s);
    
    s->methods->push(s, e);
}

Item StackPop(Stack s) {
    assert(s);
    
    return s->methods->pop(s);
}

size_t StackSize(Stack s) {
    assert(s);
    
    return s->methods->size(s);
}

bool StackIsEmpty(Stack s) {
    assert(s);
    
    return s->methods->size(s) > 0;
}

void StackClear(Stack s) {
    assert(s);
    
    s->methods->clear(s);
}

void freeStack(Stack s) {
    assert(s);
    
    s->methods->free(s);
    free(s);
}
