#include "ListSet.h"
#include "SetImpl.h"

#include "ArrayList.h"
#include "LinkedList.h"

// Impl

typedef struct createImplData {
    ItemType type;
    ListConstructor constructor;
} createImplData;

// Functions

static bool ListSetAdd(Set s, Item e) {
    List impl = (List) s->impl;
    
    if (!ListContains(impl, e)) {
        ListInsert(impl, ListSize(impl), e);
        
        return true;
    } else {
        return false;
    }
}

static Item ListSetRemove(Set s, Item e) {
    List impl = (List) s->impl;
    
    // TODO add ListRemoveItem
    size_t i = ListIndexOf(impl, e);
    if (i < ListSize(impl)) {
        return ListRemove(impl, i);
    } else {
        return NULL;
    }
}

static bool ListSetContains(Set s, Item e) {
    List impl = (List) s->impl;
    
    return ListContains(impl, e);
}

static size_t ListSetSize(Set s) {
    List impl = (List) s->impl;
    
    return ListSize(impl);    
}

static void ListSetIterate(Set s, Iterator it, void *userdata) {
    List impl = (List) s->impl;
    
    ListIterate(impl, it, userdata);
}

static void freeListSet(Set s) {
    List impl = (List) s->impl;
    
    freeList(impl);
}

static setMethods methods = {
    .add = ListSetAdd,
    .remove = ListSetRemove,
    .contains = ListSetContains,
    .size = ListSetSize,
    .iterate = ListSetIterate,
    .free = freeListSet
};

// Constructors

Set newListSet(ItemType type, ListConstructor constructor) {
    return constructSet(&methods, type, constructor(type));
}

Set newArraySet(ItemType type) {
    return newListSet(type, newArrayList);
}

Set newLinkedSet(ItemType type) {
    return newListSet(type, newLinkedList);
}
