#ifndef BINARY_SEARCH_TREE_SET_H
#define BINARY_SEARCH_TREE_SET_H

#include "Set.h"

/**
 * Create a new Set backed by a binary search tree.
 */
Set newBinarySearchTreeSet(ItemType type);

#endif
