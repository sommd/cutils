#include "ListImpl.h"

#include <stdlib.h>
#include <string.h>
#include <assert.h>

List constructList(ListMethods methods, ItemType type, void *impl) {
    assert(methods);
    assert(type);
    
    List new = NULL;
    
    if (impl) {
        // Create list
        list l = {
            .methods = methods,
            .type = type,
            .impl = impl
        };
        
        // Allocate List
        new = calloc(1, sizeof(list));
        if (new) {
            // Success, return list
            memcpy(new, &l, sizeof(list));
        } else {
            // Failed, free list and return NULL
            methods->free(&l);
        }
    }
    
    return new;
}

void ListInsert(List l, size_t i, Item e) {
    assert(l);
    assert(i <= l->methods->size(l));
    
    l->methods->insert(l, i, e);
}

Item ListGet(List l, size_t i) {
    assert(l);
    assert(i < l->methods->size(l));
    
    return l->methods->get(l, i);
}

Item ListSet(List l, size_t i, Item e) {
    assert(l);
    assert(i < l->methods->size(l));
    
    return l->methods->set(l, i, e);
}

Item ListRemove(List l, size_t i) {
    assert(l);
    assert(i < l->methods->size(l));
    
    return l->methods->remove(l, i);
}

size_t ListSize(List l) {
    assert(l);
    
    return l->methods->size(l);
}

void ListIterate(List l, Iterator it, void *userdata) {
    assert(l);
    assert(it);
    
    l->methods->iterate(l, it, userdata);
}

void freeList(List l) {
    assert(l);
    
    l->methods->free(l);
    free(l);
}

// Util functions

typedef struct countIteratorData {
    ItemType type;
    Item target;
    size_t count;
} countIteratorData;

static bool ListCountIterator(Item e, void *userdata) {
    countIteratorData *data = (countIteratorData *) userdata;
    
    if (data->type->equals(e, data->target)) {
        data->count++;
    }
    
    return true;
}

size_t ListCount(List l, Item e) {
    countIteratorData data = {
        .type = l->type,
        .target = e,
        .count = 0
    };
    
    ListIterate(l, ListCountIterator, &data);
    
    return data.count;
}

typedef struct indexOfIteratorData {
    ItemType type;
    Item target;
    size_t index;
} indexOfIteratorData;

static bool ListIndexOfIterator(Item e, void *userdata) {
    indexOfIteratorData *data = (indexOfIteratorData *) userdata;
    
    if (data->type->equals(e, data->target)) {
        return false;
    } else {
        data->index++;
        return true;
    }
}

size_t ListIndexOf(List l, Item e) {
    indexOfIteratorData data = {
        .type = l->type,
        .target = e,
        .index = 0
    };
    
    ListIterate(l, ListIndexOfIterator, &data);
    
    return data.index;
}

bool ListContains(List l, Item e) {
    return ListIndexOf(l, e) != ListSize(l);
}
