#ifndef QUEUE_H
#define QUEUE_H

#include <stdlib.h>
#include "Item.h"
#include "Iterator.h"

typedef struct queue *Queue;

typedef Queue (*QueueConstructor)(ItemType type);

void QueueEnqueue(Queue q, Item e);

Item QueueDequeue(Queue q);

size_t QueueSize(Queue q);

bool QueueIsEmpty(Queue q);

void QueueClear(Queue q);

void freeQueue(Queue q);

#endif
