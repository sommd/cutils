#include "SetImpl.h"

#include <stdlib.h>
#include <string.h>
#include <assert.h>

Set constructSet(SetMethods methods, ItemType type, void *impl) {
    assert(methods);
    assert(type);
    
    Set new = NULL;
    
    if (impl) {
        // Create set
        set s = {
            .methods = methods,
            .type = type,
            .impl = impl
        };
        
        // Allocate Set
        new = calloc(1, sizeof(set));
        if (new) {
            // Success, return set
            memcpy(new, &s, sizeof(set));
        } else {
            // Failed, free set and return NULL
            methods->free(&s);
        }
    }
    
    return new;
}

bool SetAdd(Set s, Item e) {
    assert(s);
    
    return s->methods->add(s, e);
}

Item SetRemove(Set s, Item e) {
    assert(s);
    
    return s->methods->remove(s, e);
}

bool SetContains(Set s, Item e) {
    assert(s);
    
    return s->methods->contains(s, e);
}

size_t SetSize(Set s) {
    assert(s);
    
    return s->methods->size(s);
}

void SetIterate(Set s, Iterator it, void *userdata) {
    assert(s);
    assert(it);
    
    s->methods->iterate(s, it, userdata);
}

void freeSet(Set s) {
    assert(s);
    
    s->methods->free(s);
    free(s);
}
