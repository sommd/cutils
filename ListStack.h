#ifndef LIST_STACK_H
#define LIST_STACK_H

#include "Stack.h"
#include "List.h"

Stack newListStack(ItemType type, ListConstructor constructor);

Stack newArrayStack(ItemType type);

#endif
