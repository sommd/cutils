#include "SimpleBST.h"
#include "BSTImpl.h"

#include <stdlib.h>
#include "utils.h"

typedef struct node {
    struct node *left, *right;
    Item value;
} node;

static bool SimpleBSTInsert(BST t, Item e) {
    TODO(bool);
}

static Item SimpleBSTRemove(BST t, Item e) {
    TODO(Item);
}

static Item SimpleBSTGet(BST t, Item e) {
    TODO(Item);
}

static bool SimpleBSTContains(BST t, Item e) {
    TODO(bool);
}

static size_t SimpleBSTSize(BST t) {
    TODO(size_t);
}

static size_t SimpleBSTHeight(BST t) {
    TODO(size_t);
}

static void traversePreOrder(node *n, Iterator it, void *userdata) {
    it(n->value, userdata);
    
    if (n->left) {
        traversePreOrder(n->left, it, userdata);
    }
    
    if (n->right) {
        traversePreOrder(n->left, it, userdata);
    }
}

static void traverseInOrder(node *n, Iterator it, void *userdata) {
    if (n->left) {
        traverseInOrder(n->left, it, userdata);
    }
    
    it(n->value, userdata);
    
    if (n->right) {
        traverseInOrder(n->left, it, userdata);
    }
}

static void traversePostOrder(node *n, Iterator it, void *userdata) {
    if (n->left) {
        traversePostOrder(n->left, it, userdata);
    }
    
    if (n->right) {
        traversePostOrder(n->left, it, userdata);
    }
    
    it(n->value, userdata);
}

static void traverseLevelOrder(node *n, Iterator it, void *userdata) {
    TODO(void);
}

static void SimpleBSTTraverse(BST t, Iterator it, void *userdata, bstOrder order) {
    if (t->impl) {
        void (*traverse)(node *, Iterator, void *);
        
        switch (order) {
            case PRE_ORDER:
                traverse = traversePreOrder;
                break;
            case IN_ORDER:
                traverse = traverseInOrder;
                break;
            case POST_ORDER:
                traverse = traversePostOrder;
                break;
            case LEVEL_ORDER:
                traverse = traverseLevelOrder;
                break;
        }
        
        traverse((node *) t->impl, it, userdata);
    }
}

static void freeNode(ItemType type, bool freeItem, node *n) {
    if (n->left != NULL) {
        freeNode(type, freeItem, n->left);
    }
    
    if (n->right != NULL) {
        freeNode(type, freeItem, n->right);
    }
    
    if (freeItem) {
        type->free(n->value);
    }
    
    free(n);
}

static void SimpleBSTFree(BST t) {
    if (t->impl != NULL) {
        freeNode(t->type, true, (node *) t->impl);
    }
}

static bstMethods methods = {
    .insert = SimpleBSTInsert,
    .remove = SimpleBSTRemove,
    .get = SimpleBSTGet,
    .contains = SimpleBSTContains,
    .size = SimpleBSTSize,
    .height = SimpleBSTHeight,
    .traverse = SimpleBSTTraverse,
    .free = SimpleBSTFree
};

BST newSimpleBST(ItemType type) {
    return constructBST(&methods, type, NULL);
}
