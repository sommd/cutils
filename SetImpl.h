#ifndef SET_IMPL_H
#define SET_IMPL_H

#include "Set.h"

typedef void *(*ImplConstructor)(void *userdata);

typedef const struct setMethods {
    bool (*const add)(Set s, Item e);
    Item (*const remove)(Set s, Item e);
    bool (*const contains)(Set s, Item e);
    size_t (*const size)(Set s);
    void (*const iterate)(Set s, Iterator it, void *userdata);
    void (*const free)(Set l);
} setMethods;

typedef setMethods *const SetMethods;

typedef struct set {
    /** Method table for set. */
    SetMethods methods;
    /** The ItemType of the list. */
    ItemType type;
    /** Implementation specific data for the list. */
    void *impl;
} set;

Set constructSet(SetMethods methods, ItemType type, void *impl);

#endif
