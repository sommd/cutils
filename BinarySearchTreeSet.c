#include "BinarySearchTreeSet.h"
#include "SetImpl.h"

// Impl

typedef struct bstNode {
    struct bstNode *left, *right;
    Item value;
} bstNode;

typedef struct bstSet {
    size_t size;
    bstNode *root;
} bstSet;

// Functions

static bool BinarySearchTreeSetAdd(Set s, Item e) {
    bstSet *impl = (bstSet *) s->impl;
    
    // Pointer to where new node should be put
    bstNode **newPosition = &impl->root;
    
    // Traverse tree, looking for empty place
    while (*newPosition != NULL) {
        int comp = s->type->compare(e, (*newPosition)->value);
        
        if (comp < 0) { // e < value
            // Go left
            newPosition = &((*newPosition)->left);
        } else if (comp > 0) { // e > value
            // Go right
            newPosition = &((*newPosition)->right);
        } else {
            // Don't insert
            break;
        }
    }
    
    // Insert if position is empty
    if (!*newPosition) {
        // Create node
        bstNode *new = calloc(1, sizeof(bstNode));
        new->value = e;
        
        // Insert and update size
        *newPosition = new;
        impl->size++;
        
        return true;
    } else {
        return false;
    }
}

static void bstRemove(bstNode **remove) {
    bstNode *node = *remove;
    
    if (node->left && node->right) {
        // Find immediate successor
        bstNode **successor = &node->right;
        while ((*successor)->left != NULL) {
            successor = &(*successor)->left;
        }
        
        // Swap immediate successor and node
        node->value = (*successor)->value;
        node = *successor;
        
        // Remove immediate successor
        *successor = (*successor)->right;
    } else if (node->left) {
        // Replace with left
        *remove = node->left;
    } else if (node->right) {
        // Replace with right
        *remove = node->right;
    } else {
        // Remove
        *remove = NULL;
    }
    
    free(node);
}

static Item BinarySearchTreeSetRemove(Set s, Item e) {
    bstSet *impl = (bstSet *) s->impl;
    
    bstNode **removePosition = &impl->root;
    
    // Traverse tree, looking for e
    while (*removePosition != NULL) {
        int comp = s->type->compare(e, (*removePosition)->value);
        
        if (comp < 0) { // e < value
            // Go left
            removePosition = &((*removePosition)->left);
        } else if (comp > 0) { // e > value
            // Go right
            removePosition = &((*removePosition)->right);
        } else {
            Item removed = (*removePosition)->value;
            bstRemove(removePosition);
            impl->size--;
            return removed;
        }
    }
    
    return NULL;
}

static bool BinarySearchTreeSetContains(Set s, Item e) {
    bstSet *impl = (bstSet *) s->impl;
    
    // Search tree
    bstNode *node = impl->root;
    while (node != NULL) {
        int comp = s->type->compare(e, node->value);
        
        if (comp < 0) { // e < value
            // Go left
            node = node->left;
        } else if (comp > 0) { // e > value
            // Go right
            node = node->right;
        } else {
            // Found
            return true;
        }
    }
    
    return false;
}

static size_t BinarySearchTreeSetSize(Set s) {
    return ((bstSet *) s->impl)->size;
}

static bool bstWalkInOrder(bstNode *node, Iterator it, void *userdata) {
    if (node) {
        // Do left, then node, then right, stopping when something
        // returns false
        return bstWalkInOrder(node->left, it, userdata) &&
                it(node->value, userdata) &&
                bstWalkInOrder(node->right, it, userdata);
    } else {
        // Keep walking
        return true;
    }
}

static void BinarySearchTreeSetIterate(Set s, Iterator it, void *userdata) {
    bstWalkInOrder(((bstSet *) s->impl)->root, it, userdata);
}

static void bstFree(bstNode *node) {
    if (node) {
        bstFree(node->left);
        bstFree(node->right);
        free(node);
    }
}

static void freeBinarySearchTreeSet(Set s) {
    bstSet *impl = (bstSet *) s->impl;
    
    bstFree(impl->root);
    free(impl);
}

static setMethods methods = {
    .add = BinarySearchTreeSetAdd,
    .remove = BinarySearchTreeSetRemove,
    .contains = BinarySearchTreeSetContains,
    .size = BinarySearchTreeSetSize,
    .iterate = BinarySearchTreeSetIterate,
    .free = freeBinarySearchTreeSet
};

// Constructor

Set newBinarySearchTreeSet(ItemType type) {
    return constructSet(&methods, type, calloc(1, sizeof(bstSet)));
}
