#ifndef SET_H
#define SET_H

#include <stdlib.h>
#include "Item.h"
#include "Iterator.h"

typedef struct set *Set;

typedef Set (*SetConstructor)(ItemType type);

bool SetAdd(Set s, Item e);

Item SetRemove(Set s, Item e);

bool SetContains(Set s, Item e);

size_t SetSize(Set s);

void SetIterate(Set s, Iterator it, void *userdata);

void freeSet(Set s);

#endif
