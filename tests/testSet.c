#include "testSet.h"

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "../BasicTypes.h"

#define MAX_ITEMS 100
#define NUM_DUPLICATES 5

static int randCompare(const void *a, const void *b) {
    // Return -1, 0 or 1
    return rand() % 3 - 1;
}

void testSet(SetConstructor newSet) {
    int i;
    
    // Generate items
    const int nItems = 6;
    int items[nItems][MAX_ITEMS];
    for (i = 0; i < MAX_ITEMS; i++) {
        // In order
        items[0][i] = i;
        items[3][i] = i / NUM_DUPLICATES;
        // Reverse
        items[1][MAX_ITEMS - i - 1] = i;
        items[4][MAX_ITEMS - i - 1] = i / NUM_DUPLICATES;
        // Random
        items[2][i] = i;
        items[5][i] = i / NUM_DUPLICATES;
    }
    // Shuffle random items
    qsort(items[2], MAX_ITEMS, sizeof(int), randCompare);
    qsort(items[5], MAX_ITEMS, sizeof(int), randCompare);
    
    puts("  Testing: newSet/freeSet...");
    {
        // Test new then free
        for (i = 0; i < 10; i++) {
            Set s = newSet(IntType);
            assert(s);
            freeSet(s);
        }
        
        // Test multiple new
        Set sets[10];
        for (i = 0; i < 10; i++) {
            sets[i] = newSet(IntType);
            assert(sets[i]);
        }
        // Then free
        for (i = 0; i < 10; i++) {
            freeSet(sets[i]);
        }
    }
    puts("  Passed");
    
    puts("  Testing: SetAdd...");
    {
        int j;
        for (j = 0; j < nItems; j++) {
            Set s = newSet(IntType);
            
            // Insert
            for (i = 0; i < MAX_ITEMS; i++) {
                bool expected = !SetContains(s, toIntItem(items[j][i]));                
                assert(SetAdd(s, toIntItem(items[j][i])) == expected);
            }
            
            // Check
            for (i = 0; i < MAX_ITEMS; i++) {
                assert(SetContains(s, toIntItem(items[j][i])));
            }
            
            if (j < 3) {
                assert(SetSize(s) == MAX_ITEMS);
            } else {
                assert(SetSize(s) == MAX_ITEMS / NUM_DUPLICATES);
            }
            
            freeSet(s);
        }
    }
    puts("  Passed");
    
    puts("  Testing: SetRemove...");
    {
        int j;
        for (j = 0; j < nItems; j++) {
            int k;
            for (k = 0; k < 3; k++) {
                Set s = newSet(IntType);
                
                // Insert
                for (i = 0; i < MAX_ITEMS; i++) {
                    SetAdd(s, toIntItem(items[j][i]));
                }
                
                if (j < 3) {
                    assert(SetSize(s) == MAX_ITEMS);
                } else {
                    assert(SetSize(s) == MAX_ITEMS / NUM_DUPLICATES);
                }
                
                // Remove
                for (i = 0; i < MAX_ITEMS; i++) {
                    Item item = toIntItem(items[k][i]);
                    Item expected = SetContains(s, item) ? item : NULL;
                    assert(SetRemove(s, item) == expected);
                    assert(!SetContains(s, item));
                }
                
                assert(SetSize(s) == 0);
                
                freeSet(s);
            }
        }
    }
    puts("  Passed");
    
    puts("  Testing: SetContains...");
    {
        
    }
    puts("  Passed");
    
    puts("  Testing: SetSize...");
    {
        
    }
    puts("  Passed");
    
    puts("  Testing: SetIterate...");
    {
        
    }
    puts("  Passed");
}
