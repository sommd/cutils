#include "testList.h"

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "../BasicTypes.h"

#define MAX_ITEMS 100

typedef struct testListIteratorData {
    // Value for the iterator stop at
    size_t stopAt;
} testListIteratorData;

// Current index for testListIterator
size_t testListIteratorI;

static bool testListIterator(Item e, void *userdata);

void testList(ListConstructor newList) {
    puts("  Testing: newList/freeList...");
    {
        int i;
        
        // Test new then free
        for (i = 0; i < 10; i++) {
            List l = newList(IntType);
            assert(l);
            freeList(l);
        }
        
        // Test multiple new
        List lists[10];
        for (i = 0; i < 10; i++) {
            lists[i] = newList(IntType);
            assert(lists[i]);
        }
        // Then free
        for (i = 0; i < 10; i++) {
            freeList(lists[i]);
        }
    }
    puts("  Passed");
    
    puts("  Testing: ListInsert...");
    {
        List l = newList(IntType);
        int i;
        
        // Insert at end
        for (i = 0; i < MAX_ITEMS; i++) {
            ListInsert(l, i, toIntItem(i));
            assert(fromIntItem(ListGet(l, i)) == i);
        }
        
        // Insert at start
        for (i = 0; i < MAX_ITEMS; i++) {
            ListInsert(l, 0, toIntItem(i));
            assert(fromIntItem(ListGet(l, 0)) == i);
        }
        
        // Insert after first
        for (i = 0; i < MAX_ITEMS; i++) {
            ListInsert(l, 1, toIntItem(i));
            assert(fromIntItem(ListGet(l, 1)) == i);
        }
        
        // Insert before last
        for (i = 0; i < MAX_ITEMS; i++) {
            size_t index = ListSize(l) - 1;
            ListInsert(l, index, toIntItem(i));
            assert(fromIntItem(ListGet(l, index)) == i);
        }
        
        freeList(l);
    }
    puts("  Passed");
    
    puts("  Testing: ListGet...");
    {
        List l = newList(IntType);
        int i;
        
        for (i = 0; i < MAX_ITEMS; i++) {
            ListInsert(l, i, toIntItem(i));
        }
        
        // Get in order
        for (i = 0; i < MAX_ITEMS; i++) {
            assert(fromIntItem(ListGet(l, i)) == i);
        }
        
        // Get in reverse order
        for (i = MAX_ITEMS - 1; i >= 0; i--) {
            assert(fromIntItem(ListGet(l, i)) == i);
        }
        
        // Get multiple times
        for (i = 0; i < MAX_ITEMS; i++) {
            assert(fromIntItem(ListGet(l, i)) == i);
            assert(fromIntItem(ListGet(l, i)) == i);
            assert(fromIntItem(ListGet(l, i)) == i);
        }
        
        // Get in random order
        int order[] = {
            7, 0, 43, 30, 15, 0, 84, 57, // chosen by fair dice roll.
            13, 96, 4, 94, 11, 2, 65, 66 // guaranteed to be random.
        };
        for (i = 0; i < sizeof(order) / sizeof(order[0]); i++) {
            assert(fromIntItem(ListGet(l, order[i])) == order[i]);
        }
        
        freeList(l);
    }
    puts("  Passed");
    
    puts("  Testing: ListSet...");
    {
        List l = newList(IntType);
        int i;
        
        for (i = 0; i < MAX_ITEMS; i++) {
            ListInsert(l, i, toIntItem(i));
        }
        
        // Set to reverse order
        for (i = 0; i < MAX_ITEMS; i++) {
            ListSet(l, i, toIntItem(99 - i));
            assert(fromIntItem(ListGet(l, i)) == 99 - i);
        }
        
        // Set to 4
        for (i = 0; i < MAX_ITEMS; i++) {
            ListSet(l, i,  toIntItem(4));
            assert(fromIntItem(ListGet(l, i)) == 4);
        }
        
        // Set to -1000
        for (i = 0; i < MAX_ITEMS; i++) {
            ListSet(l, i, toIntItem(-1000));
            assert(fromIntItem(ListGet(l, i)) == -1000);
        }
        
        freeList(l);
    }
    puts("  Passed");
    
    puts("  Testing: ListRemove...");
    {
        List l = newList(IntType);
        int i;
        
        for (i = 0; i < MAX_ITEMS; i++) {
            ListInsert(l, i, toIntItem(i));
        }
        
        // Remove from top
        for (i = MAX_ITEMS - 1; i >= 0; i--) {
            assert(fromIntItem(ListRemove(l, i)) == i);
            assert(ListSize(l) == i);
        }
        
        for (i = 0; i < MAX_ITEMS; i++) {
            ListInsert(l, i, toIntItem(i));
        }
        
        // Remove from bottom
        for (i = 0; i < MAX_ITEMS; i++) {
            assert(fromIntItem(ListRemove(l, 0)) == i);
            assert(ListSize(l) == 99 - i);
        }
        
        for (i = 0; i < MAX_ITEMS; i++) {
            ListInsert(l, i, toIntItem(i));
        }
        
        // Remove from middle
        for (i = 0; i < MAX_ITEMS; i++) {
            size_t index = ListSize(l) / 2;
            int expected = fromIntItem(ListGet(l, index));
            assert(fromIntItem(ListRemove(l, index)) == expected);
            assert(ListSize(l) == 99 - i);
        }
        
        freeList(l);
    }
    puts("  Passed");
    
    puts("  Testing: ListSize...");
    {
        List l = newList(IntType);
        int i;
        
        // Insert
        for (i = 0; i < MAX_ITEMS; i++) {
            assert(ListSize(l) == i);
            ListInsert(l, i, toIntItem(i));
            assert(ListSize(l) == i + 1);
        }
        
        // Remove
        for (i = MAX_ITEMS - 1; i >= 0; i--) {
            assert(ListSize(l) == i + 1);
            ListRemove(l, i);
            assert(ListSize(l) == i);
        }
        
        freeList(l);
    }
    puts("  Passed");
    
    puts("  Testing: ListIterate...");
    {
        List l = newList(IntType);
        int i;
        
        for (i = 0; i < MAX_ITEMS; i++) {
            ListInsert(l, i, toIntItem(i));
        }
        
        // Test no data
        testListIteratorI = 0;
        ListIterate(l, testListIterator, NULL);
        assert(testListIteratorI == MAX_ITEMS);
        
        testListIteratorData data;
        
        // Test no stop
        data.stopAt = 1000;
        testListIteratorI = 0;
        ListIterate(l, testListIterator, &data);
        assert(testListIteratorI == MAX_ITEMS);
        
        // Test stop at end
        data.stopAt = MAX_ITEMS - 1;
        testListIteratorI = 0;
        ListIterate(l, testListIterator, &data);
        assert(testListIteratorI == MAX_ITEMS);
        
        // Test stop at middle
        data.stopAt = MAX_ITEMS / 2;
        testListIteratorI = 0;
        ListIterate(l, testListIterator, &data);
        assert(testListIteratorI == MAX_ITEMS / 2 + 1);
        
        // Test stop at first
        data.stopAt = 0;
        testListIteratorI = 0;
        ListIterate(l, testListIterator, &data);
        assert(testListIteratorI == 1);
        
        freeList(l);
    }
    puts("  Passed");
}

static bool testListIterator(Item e, void *userdata) {
    testListIteratorData *data = (testListIteratorData *) userdata;
    size_t i = testListIteratorI++;
    
    // Check element is correct
    assert(fromIntItem(e) == (int) i);
    
    if (data) {
        // Stop when at testListIteratorStop
        return i != data->stopAt;
    } else {
        return true;
    }
}
