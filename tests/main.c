#include <stdio.h>
#include <stdlib.h>

// List
#include "testList.h"
#include "../ArrayList.h"
#include "../LinkedList.h"

// Set
#include "testSet.h"
#include "../ListSet.h"
#include "../BinarySearchTreeSet.h"

int main(int argc, char **argv) {
    puts("Testing: ArrayList");
    testList(newArrayList);
    
    puts("Testing: LinkedList");
    testList(newLinkedList);
    
    puts("Testing: ArraySet");
    testSet(newArraySet);
    
    puts("Testing: LinkedSet");
    testSet(newLinkedSet);
    
    puts("Testing: BinarySearchTreeSet");
    testSet(newBinarySearchTreeSet);
    
    return EXIT_SUCCESS;
}
