#ifndef LIST_QUEUE_H
#define LIST_QUEUE_H

#include "Queue.h"
#include "List.h"

Queue newListQueue(ItemType type, ListConstructor constructor);

Queue newLinkedQueue(ItemType type);

#endif
