#ifndef LIST_IMPL_H
#define LIST_IMPL_H

#include "List.h"

typedef const struct listMethods {
    void (*const insert)(List l, size_t i, Item e);
    Item (*const get)(List l, size_t i);
    Item (*const set)(List l, size_t i, Item e);
    Item (*const remove)(List l, size_t i);
    size_t (*const size)(List l);
    void (*const iterate)(List l, Iterator it, void *userdata);
    void (*const free)(List l);
} listMethods;

typedef listMethods *const ListMethods;

typedef struct list {
    /** Method table for list. */
    ListMethods methods;
    /** The ItemType of the list. */
    ItemType type;
    /** Implementation specific data for the list. */
    void *impl;
} list;

List constructList(ListMethods methods, ItemType type, void *impl);

#endif
