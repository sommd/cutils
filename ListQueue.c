#include "ListQueue.h"
#include "QueueImpl.h"

#include "LinkedList.h"
#include "utils.h"

static void ListQueueEnqueue(Queue q, Item e) {
    ListInsert((List) q->impl, 0, e);
}

static Item ListQueueDequeue(Queue q) {
    return ListRemove((List) q->impl, ListSize((List) q->impl) - 1);
}

static size_t ListQueueSize(Queue q) {
    return ListSize((List) q->impl);
}

static void ListQueueClear(Queue q) {
    TODO(void);
}

static void freeListQueue(Queue q) {
    freeList((List) q->impl);
}

static queueMethods methods = {
    .enqueue = ListQueueEnqueue,
    .dequeue = ListQueueDequeue,
    .size = ListQueueSize,
    .clear = ListQueueClear,
    .free = freeListQueue
};

Queue newListQueue(ItemType type, ListConstructor constructor) {
    return constructQueue(&methods, type, constructor(type));
}

Queue newLinkedQueue(ItemType type) {
    return newListQueue(type, newLinkedList);
}
