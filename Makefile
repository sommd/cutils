CC=gcc
CFLAGS=-Wall -Werror -g --std=gnu90
BINDIR=bin
OUT=test

SRCS=$(wildcard *.c) $(wildcard **/*.c)
DEPS=$(wildcard *.h) $(wildcard **/*.h)
OBJS=$(patsubst %.c,$(BINDIR)/%.o,$(SRCS))

$(OUT): $(OBJS)
	$(CC) $(CFLAGS) $^ -o $@

$(BINDIR)/%.o: %.c $(DEPS)
	@mkdir -p $(dir $@)
	$(CC) $(CFLAGS) -c $< -o $@

.PHONY: clean
clean:
	rm -f $(OUT) $(OBJS)
