#include "QueueImpl.h"

#include <stdlib.h>
#include <string.h>
#include <assert.h>

Queue constructQueue(QueueMethods methods, ItemType type, void *impl) {
    assert(methods);
    assert(type);
    
    Queue new = NULL;
    
    if (impl) {
        queue q = {
            .methods = methods,
            .type = type,
            .impl = impl
        };
        
        new = calloc(1, sizeof(queue));
        if (new) {
            memcpy(new, &q, sizeof(queue));
        } else {
            methods->free(&q);
        }
    }
    
    return new;
}

void QueueEnqueue(Queue q, Item e) {
    assert(q);
    
    q->methods->enqueue(q, e);
}

Item QueueDequeue(Queue q) {
    assert(!QueueIsEmpty(q));
    
    return q->methods->dequeue(q);
}

size_t QueueSize(Queue q) {
    assert(q);
    
    return q->methods->size(q);
}

bool QueueIsEmpty(Queue q) {
    return QueueSize(q) > 0;
}

void QueueClear(Queue q) {
    assert(q);
    
    q->methods->clear(q);
}

void freeQueue(Queue q) {
    assert(q);
    
    q->methods->free(q);
    free(q);
}
