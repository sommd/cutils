#ifndef LINKED_LIST_H
#define LINKED_LIST_H

#include "List.h"

/**
 * Create a new List backed by a doubly linked list.
 */
List newLinkedList(ItemType type);

#endif
