#ifndef BINARY_TREE_H
#define BINARY_TREE_H

#include <stdlib.h>
#include "Item.h"
#include "Iterator.h"

typedef struct bst *BST;

typedef BST (*BSTConstructor)(ItemType type);

/**
 * Insert e into the BST. If the e is not already in the tree, it
 * will be added and true will be returned. If the e does exist in the
 * tree, the tree will not be modified and false will be return.
 */
bool BSTInsert(BST t, Item e);

/**
 * Remove e from the BST. If e is in the tree, it will be removed
 * and returned without being free. If e is not in the tree, the tree
 * will not be modified and NULL will be returned.
 */
Item BSTRemove(BST t, Item e);

/**
 * Get the instance of e in the tree. If an instance of e is in the
 * tree, it will be returned. If e is not in the tree, NULL will be
 * returned.
 */
Item BSTGet(BST t, Item e);

/**
 * Returns true if an e is in the tree, false if not.
 */
bool BSTContains(BST t, Item e);

/**
 * Return the size of the tree, i.e. the number of nodes in the tree.
 */
size_t BSTSize(BST t);

/**
 * Returns the heigh of the tree.
 */
size_t BSTHeight(BST t);

/**
 * The order for the nodes in the tree to be traversed by
 * BSTTraverse().
 */
typedef enum bstOrder {
    PRE_ORDER,
    IN_ORDER,
    POST_ORDER,
    LEVEL_ORDER
} bstOrder;

/**
 * Traverse the tree in th specified order, calling it for every node in
 * the tree until it returns false.
 */
void BSTTraverse(BST t, Iterator it, void *userdata, bstOrder order);

void freeBST(BST t);

#endif
