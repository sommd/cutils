#ifndef QUEUE_IMPL_H
#define QUEUE_IMPL_H

#include "Queue.h"

typedef const struct queueMethods {
    void (*enqueue)(Queue q, Item e);
    Item (*dequeue)(Queue q);
    size_t (*size)(Queue q);
    void (*clear)(Queue q);
    void (*free)(Queue q);
} queueMethods;

typedef queueMethods *const QueueMethods;

typedef struct queue {
    QueueMethods methods;
    ItemType type;
    void *impl;
} queue;

Queue constructQueue(QueueMethods methods, ItemType type, void *impl);

#endif
