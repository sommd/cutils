#ifndef LIST_H
#define LIST_H

#include <stdlib.h>
#include "Item.h"
#include "Iterator.h"

typedef struct list *List;

typedef List (*ListConstructor)(ItemType type);

/**
* Insert item such that it's index will be i.
*/
void ListInsert(List l, size_t i, Item e);

/**
* Return the item at i. i must be less than size.
*/
Item ListGet(List l, size_t i);

/**
* Set the item at i and return the item that was at i. i must be
* less than size.
*/
Item ListSet(List l, size_t i, Item e);

/**
* Remove the item at i and return it. i must be less than size.
*/
Item ListRemove(List l, size_t i);

/**
* Return the size of the list.
*/
size_t ListSize(List l);

/**
 * Iterates over the list calling it with every element in the list
 * until it returns false.
 */
void ListIterate(List l, Iterator it, void *userdata);

/**
* Frees the list and all its elements.
*/
void freeList(List l);

// Util functions

/**
 * Count the number of instances of e in the list.
 */
size_t ListCount(List l, Item e);

/**
 * Return the index of the first instance of e in the list. If there are
 * no instances of e in the list, the size of the list is returned.
 */
size_t ListIndexOf(List l, Item e);

/**
 * Returns true if the list contains at least one instance of e.
 */
bool ListContains(List l, Item e);

#endif
