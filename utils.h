#ifndef UTILS_H
#define UTILS_H

#include <stdlib.h>
#include <stdio.h>

#define TODO(type) \
    fprintf(stderr, "Unimplemented: %s:%d\n", __FILE__, __LINE__); \
    exit(EXIT_FAILURE); \
    return ((type) NULL)

// OS Identification

#if defined(__unix__) || defined(__unix) || defined(unix)
    #define OS_UNIX
    #define OS_UNIX_LIKE
#endif

#if defined(__linux__) || defined(__linux) || defined(linux)
    #define OS_LINUX
    #define OS_UNIX_LIKE
#endif

#if defined(__CYGWIN__) || defined(__MINGW32__)
    #define OS_UNIX_LIKE
#endif

#if defined(_WIN32) || defined (_WIN64)
    #define OS_WINDOWS
#endif

// String

#if defined(OS_UNIX_LIKE)
    #include <strings.h>
    #define strcmpIgnoreCase(a, b) (strcasecmp(a, b))
#elif defined(OS_WINDOWS)
    #include <string.h>
    #define strcmpIgnoreCase(a, b) (stricmp(a, b))
#endif

#endif
