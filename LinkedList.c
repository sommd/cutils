#include "LinkedList.h"
#include "ListImpl.h"

#include <stdlib.h>
#include <assert.h>

// Impl

typedef struct listNode {
    struct listNode *next, *prev;
    Item value;
} listNode;

typedef struct linkedList {
    size_t size;
    listNode *first, *last;
} linkedList;

// Functions

static listNode *getNode(linkedList *impl, size_t i) {
    int j;
    listNode *it;
    
    if (i >= impl->size) {
        return NULL;
    } else if (i <= impl->size / 2) {
        for (j = 0, it = impl->first; true; j++, it = it->next) {
            if (j == i) {
                return it;
            }
        }
    } else {
        for (j = impl->size - 1, it = impl->last; true; j--, it = it->prev) {
            if (j == i) {
                return it;
            }
        }
    }
}

static void linkNodes(listNode *left, listNode *right) {
    if (left) {
        left->next = right;
    }
    if (right) {
        right->prev = left;
    }
}

// Methods

static void LinkedListInsert(List l, size_t i, Item e) {
    linkedList *impl = (linkedList *) l->impl;
    
    listNode *new = calloc(1, sizeof(listNode));
    assert(new);
    
    new->value = e;
    
    if (i == 0 && i == impl->size) {
        impl->first = impl->last = new;
    } else if (i == 0) {
        linkNodes(new, impl->first);
        impl->first = new;
    } else if (i == impl->size) {
        linkNodes(impl->last, new);
        impl->last = new;
    } else {
        listNode *old = getNode(impl, i);
        linkNodes(old->prev, new);
        linkNodes(new, old);
    }
    
    impl->size++;
}

static Item LinkedListGet(List l, size_t i) {
    return getNode((linkedList *) l->impl, i)->value;
}

static Item LinkedListSet(List l, size_t i, Item e) {
    listNode *node = getNode((linkedList *) l->impl, i);
    
    Item old = node->value;
    node->value = e;
    
    return old;
}

static Item LinkedListRemove(List l, size_t i) {
    linkedList *impl = (linkedList *) l->impl;
    
    listNode *node = getNode(impl, i);
    
    if (node->prev) {
        node->prev->next = node->next;
    }
    
    if (node->next) {
        node->next->prev = node->prev;
    }
    
    if (i == 0) {
        impl->first = node->next;
    }
    
    if (i == impl->size - 1) {
        impl->last = node->prev;
    }
    
    impl->size--;
    
    return node->value;
}

static size_t LinkedListSize(List l) {
    return ((linkedList *) l->impl)->size;
}

static void LinkedListIterate(List l, Iterator it, void *userdata) {
    linkedList *impl = (linkedList *) l->impl;
    
    // Iterate through nodes
    listNode* node;
    size_t i;
    for (i = 0, node = impl->first; node != NULL; i++, node = node->next) {
        // Break if it returns false
        if (!it(node->value, userdata)) {
            break;
        }
    }
}

static void freeLinkedList(List l) {
    // TODO
}

static listMethods methods = {
    .insert = LinkedListInsert,
    .get = LinkedListGet,
    .set = LinkedListSet,
    .remove = LinkedListRemove,
    .size = LinkedListSize,
    .iterate = LinkedListIterate,
    .free = freeLinkedList
};

// Constructors

List newLinkedList(ItemType type) {
    linkedList *impl = calloc(1, sizeof(linkedList));
    return constructList(&methods, type, impl);
}
