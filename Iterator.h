#ifndef ITERATOR_H
#define ITERATOR_H

#include "Item.h"

typedef bool (*Iterator)(Item item, void *userdata);

#endif
