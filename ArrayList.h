#ifndef ARRAY_LIST_H
#define ARRAY_LIST_H

#include "List.h"

/**
 * Create a new List backed by a dynamically sized array, starting with
 * size of preallocate.
 */
List newArrayListPreallocated(ItemType type, size_t preallocate);

/**
 * Create a new List backed by a dynamically sized array.
 */
List newArrayList(ItemType type);

#endif
