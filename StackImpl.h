#ifndef STACK_IMPL_H
#define STACK_IMPL_H

#include "Stack.h"

typedef const struct stackMethods {
    void (*push)(Stack s, Item e);
    Item (*pop)(Stack s);
    size_t (*size)(Stack s);
    void (*clear)(Stack s);
    void (*free)(Stack s);
} stackMethods;

typedef stackMethods *const StackMethods;

typedef struct stack {
    StackMethods methods;
    ItemType type;
    void *impl;
} stack;

Stack constructStack(StackMethods methods, ItemType type, void *impl);

#endif
