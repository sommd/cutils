#include "BSTImpl.h"

#include <stdlib.h>
#include <string.h>
#include <assert.h>

BST constructBST(BSTMethods methods, ItemType type, void *impl) {
    assert(methods);
    assert(type);
    
    BST new = NULL;
    
    if (impl) {
        // Create bst
        bst t = {
            .methods = methods,
            .type = type,
            .impl = impl
        };
        
        // Allocate BST
        new = calloc(1, sizeof(bst));
        if (new) {
            // Success, return set
            memcpy(new, &t, sizeof(bst));
        } else {
            // Failed, free set and return NULL
            methods->free(&t);
        }
    }
    
    return new;
}

bool BSTInsert(BST t, Item e) {
    assert(t);
    
    return t->methods->insert(t, e);
}

Item BSTRemove(BST t, Item e) {
    assert(t);
    
    return t->methods->remove(t, e);
}

Item BSTGet(BST t, Item e) {
    assert(t);
    
    return t->methods->get(t, e);
}

bool BSTContains(BST t, Item e) {
    assert(t);
    
    return t->methods->contains(t, e);
}

size_t BSTSize(BST t) {
    assert(t);
    
    return t->methods->size(t);
}

size_t BSTHeight(BST t) {
    assert(t);
    
    return t->methods->height(t);
}

void BSTTraverse(BST t, Iterator it, void *userdata, bstOrder order) {
    assert(t);
    assert(it);
    assert(order >= PRE_ORDER && order <= LEVEL_ORDER);
    
    return t->methods->traverse(t, it, userdata, order);
}

void freeBST(BST t) {
    assert(t);
    
    t->methods->free(t);
    free(t);
}
