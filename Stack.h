#ifndef STACK_H
#define STACK_H

#include <stdlib.h>
#include "Item.h"
#include "Iterator.h"

typedef struct stack *Stack;

typedef Stack (*StackConstructor)(ItemType type);

void StackPush(Stack s, Item e);

Item StackPop(Stack s);

size_t StackSize(Stack s);

bool StackIsEmpty(Stack s);

void StackClear(Stack s);

void freeStack(Stack s);

#endif
