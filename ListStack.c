#include "ListStack.h"
#include "StackImpl.h"

#include "ArrayList.h"
#include "utils.h"

static void ListStackPush(Stack q, Item e) {
    ListInsert((List) q->impl, ListSize((List) q->impl), e);
}

static Item ListStackPop(Stack q) {
    return ListRemove((List) q->impl, ListSize((List) q->impl) - 1);
}

static size_t ListStackSize(Stack q) {
    return ListSize((List) q->impl);
}

static void ListStackClear(Stack q) {
    TODO(void);
}

static void freeListStack(Stack q) {
    freeList((List) q->impl);
}

static stackMethods methods = {
    .push = ListStackPush,
    .pop = ListStackPop,
    .size = ListStackSize,
    .clear = ListStackClear,
    .free = freeListStack
};

Stack newListStack(ItemType type, ListConstructor constructor) {
    return constructStack(&methods, type, constructor(type));
}

Stack newArrayStack(ItemType type) {
    return newListStack(type, newArrayList);
}
