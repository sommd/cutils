#ifndef BASIC_TYPES_H
#define BASIC_TYPES_H

#include <stdint.h>
#include "Item.h"

// Int

extern ItemType IntType;

inline Item toIntItem(intptr_t i);
inline intptr_t fromIntItem(Item i);

// UInt

extern ItemType UIntType;

inline Item toUIntItem(uintptr_t u);
inline uintptr_t fromUIntItem(Item i);

// String

extern ItemType StringType;
extern ItemType CaseInsensitiveStringType;

inline Item toStringItem(char *s);
inline char *fromStringItem(Item i);

#endif
