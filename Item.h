#ifndef ITEM_H
#define ITEM_H

#include <stdbool.h>

typedef void *Item;

typedef const struct itemType {
    int (*compare)(Item, Item);
    bool (*equals)(Item, Item);
    void (*free)(Item);
} itemType;

// Constant pointer to constant itemType
typedef const itemType *const ItemType;

#endif
