#include "BasicTypes.h"

#include <stdlib.h>
#include <string.h>
#include "utils.h"

static void NoopFree(Item i) {}

// Int

static int IntCompare(Item a, Item b) {
    return fromIntItem(a) - fromIntItem(b);
}

static bool IntEquals(Item a, Item b) {
    return a == b;
}

ItemType IntType = &((itemType) {
    .compare = IntCompare,
    .equals = IntEquals,
    .free = NoopFree
});

inline Item toIntItem(intptr_t i) {
    return (Item) i;
}

inline intptr_t fromIntItem(Item i) {
    return (intptr_t) i;
}

// UInt

static int UIntCompare(Item a, Item b) {
    return fromUIntItem(a) - fromUIntItem(b);
}

static bool UIntEquals(Item a, Item b) {
    return a == b;
}

ItemType UIntType = &((itemType) {
    .compare = UIntCompare,
    .equals = UIntEquals,
    .free = NoopFree
});

inline Item toUIntItem(uintptr_t i) {
    return (Item) i;
}

inline uintptr_t fromUIntItem(Item i) {
    return (uintptr_t) i;
}

// String

static int StringCompare(Item a, Item b) {
    return strcmp(fromStringItem(a), fromStringItem(b));
}

static bool StringEquals(Item a, Item b) {
    return strcmp(fromStringItem(a), fromStringItem(b)) == 0;
}

ItemType StringType = &((itemType) {
    .compare = StringCompare,
    .equals = StringEquals,
    .free = free
});

static int CaseInsensitiveStringCompare(Item a, Item b) {
    return strcmpIgnoreCase(fromStringItem(a), fromStringItem(b));
}

static bool CaseInsensitiveStringEquals(Item a, Item b) {
    return strcmpIgnoreCase(fromStringItem(a), fromStringItem(b)) == 0;
}

ItemType CaseInsensitiveStringType = &((itemType) {
    .compare = CaseInsensitiveStringCompare,
    .equals = CaseInsensitiveStringEquals,
    .free = free
});

inline Item toStringItem(char *s) {
    return strdup(s);
}

inline char *fromStringItem(Item i) {
    return (char *) i;
}
