#ifndef BINARY_TREE_IMPL_H
#define BINARY_TREE_IMPL_H

#include "BST.h"

typedef const struct bstMethods {
    bool (*insert)(BST t, Item e);
    Item (*remove)(BST t, Item e);
    Item (*get)(BST t, Item e);
    bool (*contains)(BST t, Item e);
    size_t (*size)(BST t);
    size_t (*height)(BST t);
    void (*traverse)(BST t, Iterator it, void *userdata, bstOrder order);
    void (*free)(BST t);
} bstMethods;

typedef bstMethods *const BSTMethods;

typedef struct bst {
    /** Method table for binary tree. */
    BSTMethods methods;
    /** The ItemType of the binary tree. */
    ItemType type;
    /** Implementation specific data for the binary tree. */
    void *impl;
} bst;

BST constructBST(BSTMethods methods, ItemType type, void *impl);

#endif
