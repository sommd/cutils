#include "ArrayList.h"
#include "ListImpl.h"

#include <stdlib.h>
#include <assert.h>

#define DEFAULT_ARRAY_SIZE 8
#define GROWTH_FACTOR 1.5

// Impl

typedef struct arrayList {
    // Number of elements in list
    size_t size;
    // Size of array
    size_t arraySize;
    // Array of elements
    Item array[];
} arrayList;

typedef struct createImplData {
    size_t preallocate;
} createImplData;

// Methods

static void ArrayListInsert(List l, size_t i, Item e) {
    arrayList *impl = (arrayList *) l->impl;
    
    // Resize impl if needed
    if (impl->arraySize == impl->size) {
        // Calculate new size
        size_t newSize = impl->arraySize * GROWTH_FACTOR;
        
        // Resize impl
        impl = l->impl = realloc(impl, sizeof(arrayList) + newSize * sizeof(Item));
        assert(impl); // Make sure we didn't run out of memory
        
        // Update arraySize
        impl->arraySize = newSize;
    }
    
    // Shift elements forward
    int j;
    for (j = impl->size; j > i; j--) {
        impl->array[j] = impl->array[j - 1];
    }
    
    // Insert new element
    impl->array[i] = e;
    
    // Update size
    impl->size++;
}

static Item ArrayListGet(List l, size_t i) {
    return ((arrayList *) l->impl)->array[i];
}

static Item ArrayListSet(List l, size_t i, Item e) {
    arrayList *impl = (arrayList *) l->impl;
    
    // Get old element
    Item old = impl->array[i];
    
    // Set new
    impl->array[i] = e;
    
    return old;
}

static Item ArrayListRemove(List l, size_t i) {
    arrayList *impl = (arrayList *) l->impl;
    
    // Get old element
    Item old = impl->array[i];
    
    // Shift elements back
    for (i++; i < impl->size; i++) {
        impl->array[i - 1] = impl->array[i];
    }
    
    // Update size
    impl->size--;
    
    return old;
}

static size_t ArrayListSize(List l) {
    return ((arrayList *) l->impl)->size;
}

static void ArrayListIterate(List l, Iterator it, void *userdata) {
    arrayList *impl = (arrayList *) l->impl;
    
    // Iterate over array
    int i;
    for (i = 0; i < impl->size; i++) {
        // Break if it returns false
        if (!it(impl->array[i], userdata)) {
            break;
        }
    }
}

static void freeArrayList(List l) {
    arrayList *impl = (arrayList *) l->impl;
    
    // Free each element
    int i;
    for (i = 0; i < impl->size; i++) {
        l->type->free(impl->array[i]);
    }
    
    // Free impl
    free(impl);
}

static listMethods methods = {
    .insert = ArrayListInsert,
    .get = ArrayListGet,
    .set = ArrayListSet,
    .remove = ArrayListRemove,
    .size = ArrayListSize,
    .iterate = ArrayListIterate,
    .free = freeArrayList
};

// Constructors

List newArrayListPreallocated(ItemType type, size_t preallocate) {
    arrayList *impl = calloc(1, sizeof(arrayList) + preallocate * sizeof(Item));
    
    if (impl) {
        impl->arraySize = preallocate;
    }
    
    return constructList(&methods, type, impl);
}

List newArrayList(ItemType type) {
    return newArrayListPreallocated(type, DEFAULT_ARRAY_SIZE);
}
