#ifndef LIST_SET_H
#define LIST_SET_H

#include "Set.h"
#include "List.h"

/**
 * Create a new Set backed by a List.
 */
Set newListSet(ItemType type, ListConstructor constructor);

/**
 * Create a new Set backed by an ArrayList.
 */
Set newArraySet(ItemType type);

/**
 * Create a new Set backed by an LinkedList.
 */
Set newLinkedSet(ItemType type);

#endif
